//Khai báo thư viện express:
const express = require('express');
const { createPhoto, getAllPhotos, getPhotoById, updatePhotoById, deletePhotoById, getPhotoOfAlbum } = require('../controllers/photoController');

//Khai báo  router:
const photoRouter = express.Router();


photoRouter.post("/photos",createPhoto );

photoRouter.get("/photos", getAllPhotos);

photoRouter.get("/photos/:photoId", getPhotoById);

photoRouter.put("/photos/:photoId", updatePhotoById);

photoRouter.delete("/photos/:photoId", deletePhotoById);

photoRouter.get("/albums/:albumId/photos", getPhotoOfAlbum);




module.exports = { photoRouter }