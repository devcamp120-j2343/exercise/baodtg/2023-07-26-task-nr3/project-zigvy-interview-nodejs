//Khai báo thư viện express:
const express = require('express');
const { createNewComment, getAllComments, getCommentById, updateCommentById, deleteCommentById, getCommentOfPost } = require('../controllers/commentControllers');

//Khai báo  router:
const commentRouter = express.Router();


commentRouter.post("/comments", createNewComment)

commentRouter.get("/comments", getAllComments)

commentRouter.get("/comments/:commentId", getCommentById)

commentRouter.put("/comments/:commentId", updateCommentById)

commentRouter.delete("/comments/:commentId", deleteCommentById)

commentRouter.get("/posts/:postId/comments", getCommentOfPost)


module.exports = { commentRouter }