//Khai báo thư viện express:
const express = require('express');
const { createToDo, getAllToDos, getToDoById, updateToDoById, deleteToDoById, getToDoOfUser } = require('../controllers/toDoController');

//Khai báo  router:
const toDoRouter = express.Router();


toDoRouter.post("/toDos", createToDo);

toDoRouter.get("/toDos", getAllToDos);

toDoRouter.get("/toDos/:toDoId", getToDoById);

toDoRouter.put("/toDos/:toDoId", updateToDoById);

toDoRouter.delete("/toDos/:toDoId", deleteToDoById);

toDoRouter.get("/users/:userId/toDos", getToDoOfUser);




module.exports = {toDoRouter }