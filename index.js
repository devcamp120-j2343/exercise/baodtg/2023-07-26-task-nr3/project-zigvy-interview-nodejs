//Khai báo thư viện express:
const express = require('express');

//Khai báo thư viện mongoose:
const mongoose = require('mongoose')

//import models:
const userModel = require("./app/models/userModel");
const postModel = require("./app/models/postModel");
const commentModel = require("./app/models/commentModel");
const albumModel = require("./app/models/albumModel")
const photoModel = require("./app/models/photoModel")
const toDoModel = require("./app/models/toDoModel")

//import routers:
const { userRouter } = require('./app/routers/userRouter');
const { postRouter } = require('./app/routers/postRouter');
const { commentRouter } = require('./app/routers/commentRouter');
const { albumRouter } = require('./app/routers/albumRouter');
const { photoRouter } = require('./app/routers/photoRouter');
const { toDoRouter } = require('./app/routers/toDoRouter');


//Khai báo app:
const app = express();

//Khai báo cổng:
const port = 8000;

//Kết nối mongoDB:
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Zigvy_Interview")
.then(() => console.log("Connected to Mongo Successfully"))
.catch(error => handleError(error));

app.use(express.json())

app.use("/", userRouter);
app.use("/", postRouter);
app.use("/", commentRouter);
app.use("/", albumRouter);
app.use("/", photoRouter);
app.use("/", toDoRouter);

app.listen(port, () => {
    console.log(`Khởi chạy app trên cổng ${port}`)
})